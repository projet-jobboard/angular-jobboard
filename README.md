# Angujobboard    

## Description    

(Travail en cours)    
Cette application est la partie front d'un Job-board, développée pour fonctionner avec l'[API jobboard](https://gitlab.com/projet-jobboard/api-jobboard) faite avec MySQL, Node, Express et Sequelize.        



## Structure du projet    
```
src---app---auth---login    
  |     |     |----register    
  |     |---candiates-----one-candidate    
  |     |     |-----------update-candidate    
  |     |     |-----------delete-candidate    
  |     |---enterprises---one-enterprise    
  |     |     |-----------update-enterprise    
  |     |     |-----------delete-enterprise    
  |     |---accueil    
  |     |---models    
  |     |---offers--------one-offer    
  |     |     |-----------update-offer    
  |     |     |-----------delete-offer    
  |     |---services    
  |     |---static-pages---mentions    
  |     |     |------------contact    
  |     |---users---one-user    
  |     |     |-----update-user    
  |     |     |-----delete-user     
  |---assets---images    
  |     |------pdf    
  |---favicon.ico    
  |---index.html    
  |---main.ts    
  |---styles.css    
angular.json    
README.md    
```    

## Technique        
    
Utilisation du framework [Angular](http://angular.io)    

### Ajouts spécifiques à ce projet

**Intégrer Bootstrap**
*  Taper dans la console, à la racine du projet : `npm install --save bootstrap` pour installer le node_module bootstrap dans le projet
*  Dans le fichier angular.json ajouter Bootstrap pour qu'il soit pris en compte :

  ```
  (...)
  "architect": {
    "build": {
      "builder": "@angular-devkit/build-angular:browser",
      "options": {
        "outputPath": "dist/angujobboard",
        "index": "src/index.html",
        "main": "src/main.ts",
        "polyfills": "src/polyfills.ts",
        "tsConfig": "tsconfig.app.json",
        "aot": true,
        "assets": [
          "src/favicon.ico",
          "src/assets"
        ],
            (Dans styles et scripts : )
        "styles": [
          "node_modules/bootstrap/dist/css/bootstrap.min.css"
        ],
        "scripts": [
          "node_modules/jquery/dist/jquery.min.js",
          "node_modules/bootstrap/dist/js/bootstrap.min.js"
        ]
      },
      (...)
````    

**Le CSS en global**

Angular donne la possibilité d'intégrer le CSS par portions dans la structure du projet, mais je trouve que le CSS global est plus simple et rapide à implémenter, retrouver et maintenir. Le fichier CSS global est dans :    
```/src/styles.css```       
L'ajouter aussi à angular.json et redémarrer le serveur    


**Ne pas installer angular-test-truncate**     
Beaucoup de dépendances non satisfaites et de déprécations.
Pour tronquer du texte, utiliser slice().



-----
-----







This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
