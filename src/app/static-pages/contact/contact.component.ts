
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {EmailService} from '../../services/email.service';
import {Email} from '../../models/email.model';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  pageTitle = 'Nous contacter';

  email: Email = {
    from: '',
    subject: '',
    message: ''
    // filename: '',
    // path: ''
  };

  isSubmit = false;

  constructor(
    private emailService: EmailService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  //  méthode appelée par le clic sur submit
  sendMail() {
    const data = {
      from: this.email.from,
      subject: this.email.subject,
      message: this.email.message
      // filename: this.email.filename,
      // path: this.email.path
    };

    //  vérifie qu'on récupère les données du formulaire
    console.log(data);

    this.emailService.send_mail(data).subscribe(() => {
      this.isSubmit = true;
      //  vérifie que les données dans data sont toujours là, en cas d'échec
      console.log(data);
    }, (err) => {
      console.log(err);
    })
  }


}
