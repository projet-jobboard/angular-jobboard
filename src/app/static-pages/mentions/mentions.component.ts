
//  importer les modules pour gérer l'utilisation des components 
//  et le lancement de l'application
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

//  importer le service dont on aura besoin
import {StaticPagesService} from '../../services/static-pages.service';


@Component({
  selector: 'app-mentions',
  templateUrl: './mentions.component.html',
  styleUrls: ['./mentions.component.css']
})
export class MentionsComponent implements OnInit {

  pageTitle = 'Mentions légales';

  mentionsPage: any;

  constructor(
    private staticPagesService: StaticPagesService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.mentions(this.route.snapshot.paramMap.get('id'));
  }

  mentions(id) {
    this.staticPagesService.one_page(id).subscribe((result) => {
      this.mentionsPage = result;
      console.log(this.mentionsPage);
    }, (err) => {
      console.log(err);
    })
  };

}


