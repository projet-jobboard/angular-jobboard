
//  importer les modules pour gérer l'utilisation des components 
//  et le lancement de l'application
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

//  importer le service dont on aura besoin
import {StaticPagesService} from '../services/static-pages.service';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']  //  ./about.component.css
})
export class AboutComponent implements OnInit {

  pageTitle = 'A propos';

  aboutPage: any;

  constructor(
    private staticPagesService: StaticPagesService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.about(this.route.snapshot.paramMap.get('id'));
  }

  about(id) {
    this.staticPagesService.one_page(id).subscribe((result) => {
      this.aboutPage = result;
      console.log(this.aboutPage);
    }, (err) => {
      console.log(err);
    })
  };

}
