
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
//  importer le module pour tronquer les textes
// import {AngularTextTruncateModule} from 'angular-text-truncate';

import {OffersComponent} from './offers//offers.component';
import { EnterprisesComponent } from './enterprises/enterprises.component';
import { UsersComponent } from './users/users.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { HomeComponent } from './home/home.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { AboutComponent } from './static-pages/about.component';
import { MentionsComponent } from './static-pages/mentions/mentions.component';
import { OneEnterpriseComponent } from './enterprises/one-enterprise/one-enterprise.component';
import { UpdateEnterpriseComponent } from './enterprises/update-enterprise/update-enterprise.component';
import { OneUserComponent } from './users/one-user/one-user.component';
import { OneCandidateComponent } from './candidates/one-candidate/one-candidate.component';
import { OneOfferComponent } from './offers/one-offer/one-offer.component';
import { ContactComponent } from './static-pages/contact/contact.component';
import { RechercheComponent } from './home/recherche/recherche.component';
import { HeaderComponent } from './templates/header/header.component';
import { FooterComponent } from './templates/footer/footer.component';
import { ApplyComponent } from './offers/apply/apply.component';
import { CreateEnterpriseComponent } from './enterprises/create-enterprise/create-enterprise.component';
import { CreateOfferComponent } from './offers/create-offer/create-offer.component';


@NgModule({
  declarations: [
    AppComponent,
    OffersComponent,
    EnterprisesComponent,
    UsersComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    CandidatesComponent,
    AboutComponent,
    MentionsComponent,
    OneEnterpriseComponent,
    UpdateEnterpriseComponent,
    OneUserComponent,
    OneCandidateComponent,
    OneOfferComponent,
    ContactComponent,
    RechercheComponent,
    HeaderComponent,
    FooterComponent,
    ApplyComponent,
    CreateEnterpriseComponent,
    CreateOfferComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
