
import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {CandidateService} from '../services/candidate.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['../app.component.css']
})
export class HomeComponent implements OnInit {

  siteTitle = 'Jobboard';
  pageTitle = 'les jobs à impact social et environnemental'

  userValue: any;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private candidateService: CandidateService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userValue = JSON.parse(localStorage.getItem('current_user'));
  }
  

}
