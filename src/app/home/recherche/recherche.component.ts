
//  importer les ngModules dont on a besoin
import { Component, OnInit } from '@angular/core';

//  importer les services et modèles dont on  besoin
import {OfferService} from '../../services/offer.service';
import {ContractService} from '../../services/contract.service';
import {SectorService} from '../../services/sector.service';
import {StatusService} from '../../services/status.service';

import {Offer} from '../../models/offer.model';
import {Contract} from '../../models/contract.model';
import {Sector} from '../../models/sector.model';
import {Status} from '../../models/status.model';



@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.css']
})
export class RechercheComponent implements OnInit {

  // pageTitle = 'Recherche';

  isSubmitted = false;
  
  //  Offers pour réponse
  // offers: Offer[];

  count_offers: number;
  count_kw: number;
  count_contracts: number;
  count_sectors: number;
  count_status: number;
  // noOffers = 'Il n\'y a pas d\'offres avec vos critères.';

  //  variables pour afficher formulaire
  contracts: Contract[];
  sectors: Sector[];
  statuss: Status[];

  //  variables à envoyer par le formulaire
  kw: string;
  cont: number;
  sect: number;
  stat: number;
  contract_type: number;
  sector_name: number;
  status_type: number;

  
  // current_search: any = {};
  current_search: Offer[];

  userValue: any;

  constructor(
    private offerService: OfferService,
    private contractService: ContractService,
    private sectorService: SectorService,
    private statusService: StatusService
  ) { }

  ngOnInit(): void {

    //  récupérer les infos du user pour afficher son nom et prénom
    this.userValue = JSON.parse(localStorage.getItem('current_user'));

    //  récupérer depuis la bdd les contrats, secteurs et statuts 
    //  pour les afficher dans les options des selects
    this.contractService.all_contracts().subscribe((contract) => {
      this.contracts = contract;
      this.count_contracts = this.contracts.length;
    });

    this.sectorService.all_sectors().subscribe((sector) => {
      this.sectors = sector;
      this.count_sectors = this.sectors.length;
    });

    this.statusService.all_status().subscribe((status) => {
      this.statuss = status;
      this.count_status = this.statuss.length;
    })

  }

  search() {

    this.offerService.offers_by_many_choices(this.kw, this.contract_type, this.sector_name, this.status_type).subscribe((result) => {
      console.log(`${this.kw}, ${this.sector_name}`);
      this.isSubmitted = true;
      this.current_search = result;
      this.count_offers = result.length;
      
    }, (err) => {
      console.log(err);
    })
  }

}


