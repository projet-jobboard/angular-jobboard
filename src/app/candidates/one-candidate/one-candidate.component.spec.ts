import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneCandidateComponent } from './one-candidate.component';

describe('OneCandidateComponent', () => {
  let component: OneCandidateComponent;
  let fixture: ComponentFixture<OneCandidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneCandidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
