
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

//  importer les services dont on a besoin
import {CandidateService} from '../../services/candidate.service';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-one-candidate',
  templateUrl: './one-candidate.component.html',
  styleUrls: ['./one-candidate.component.css']
})
export class OneCandidateComponent implements OnInit {

  pageTitle = 'Compte candidat';

  candidate: any;

  fname: string;
  lname: string;
  user_name: any;
  candidateObj: any;

  user: any;

  userValue: any;

  constructor(
    private candidateService: CandidateService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    //  récupérer les infos du user pour afficher son nom et prénom
    this.userValue = JSON.parse(localStorage.getItem('current_user'));
    // console.log(`current_user : ${this.userValue.firstname}`);
    this.getCandidate(this.route.snapshot.paramMap.get('id'));
    // console.log(`candidat : ${this.candidate.candidate_id}`);

  }

  // //  récupérer les infos du user à partir de son firstname et son lastname
  // get_user_by_username(fname, lname) {
  //   this.userService.user_by_username(this.fname, this.lname).subscribe((result) => {
  //     this.user_name = result;
  //     console.log(`user_name user_id : ${this.user_name.user_id}`);
      
  //     // this.get_ent_by_user(this.user_name.user_id);
  //     this.get_candidate_by_user(this.user_name.user_id);
      
  //   }, ((err) => {
  //     console.log(err);
  //   }))
  // }

    //  récupérer le candidat à partir du username du user stocké dans le localStorage
  getCandidate(id) {
    //  user by username
    this.userService.user_by_username(this.userValue.firstname, this.userValue.lastname).subscribe((resUser) => {
      this.user = resUser;
      console.log(resUser);
      console.log(this.user.user_id);
      
      //  candidate by user_id
      this.candidateService.cand_by_user(this.user.user_id).subscribe((result) => {
        this.candidate = result;
        console.log(result);
        console.log(this.candidate.candidate_id);
      }, (err) => {
        console.log(err);
      })
    }, (err) => {
      console.log(err);
    })
  }

  
  // //  candidat par id du user
  // get_candidate_by_user(user_id) {
  //   this.candidateService.cand_by_user(this.user_name.user_id).subscribe((result) => {
  //     this.candidateObj = result;
  //     console.log(`candidateObj : ${this.candidateObj}`);
      
  //     console.log(`candidate.fk_user_id : ${this.candidateObj.fk_user_id}`);
  //     console.log(`candidate.candidate_id : ${this.candidateObj.candidate_id}`);
  //   }, (err) => {
  //     console.log(err);
  //   })
  // }



}


