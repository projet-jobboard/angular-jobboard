
import { Component, OnInit } from '@angular/core';

//  on importe le service et le modèle dont on a besoin
import {CandidateService} from '../services/candidate.service';
import {Candidate} from '../models/candidate.model';


@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.css']
})
export class CandidatesComponent implements OnInit {

  candidates: Candidate[];
  pageTitle = 'Tous les candidats';

  userValue: any;

  constructor(
    private candidateService: CandidateService
  ) { }

  ngOnInit(): void {

    this.userValue = JSON.parse(localStorage.getItem('current_user'));
    console.log(this.userValue.role);

    this.candidateService.all_candidates().subscribe((result) => {
      this.candidates = result;
      console.log(result);
    }, (err) => {
      console.log(err);
    })

  }

}
