
//  importer NgModule pour utiliser les modules
//  et Routes et RouterModule pour gérer le routing
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth.guard';
//  importer les components
// import { RechercheComponent } from './home/recherche/recherche.component';
import {ApplyComponent} from './offers/apply/apply.component';
//  user components
import { UsersComponent } from './users/users.component';
import { OneUserComponent } from './users/one-user/one-user.component';
//  authentification components
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
// home component
import { HomeComponent } from './home/home.component';
//  pages statiques
import {AboutComponent} from './static-pages/about.component';
import {MentionsComponent} from './static-pages/mentions/mentions.component';
import { ContactComponent } from './static-pages/contact/contact.component';
//  enterprise components
import { EnterprisesComponent } from './enterprises/enterprises.component';
import {OneEnterpriseComponent} from './enterprises/one-enterprise/one-enterprise.component';
//  candidat components
import { CandidatesComponent } from './candidates/candidates.component';
import { OneCandidateComponent } from './candidates/one-candidate/one-candidate.component';
//  offer components
import { OffersComponent } from './offers//offers.component';
import { OneOfferComponent } from './offers/one-offer/one-offer.component';
import { UpdateEnterpriseComponent } from './enterprises/update-enterprise/update-enterprise.component';
import { CreateEnterpriseComponent } from './enterprises/create-enterprise/create-enterprise.component';
import { CreateOfferComponent } from './offers/create-offer/create-offer.component';


const routes: Routes = [
  
  //  routes candidates
  { path: 'all_candidates', canActivate: [AuthGuard], component: CandidatesComponent },
  { path: 'one_candidate/:id', canActivate: [AuthGuard], component: OneCandidateComponent },
  
  //  routes offers
  { path: 'create_offer', canActivate: [AuthGuard], component: CreateOfferComponent },
  { path: 'all_offers', canActivate: [AuthGuard], component: OffersComponent},
  { path: 'one_offer/:id', canActivate: [AuthGuard], component: OneOfferComponent },
  { path: 'apply/:id', component: ApplyComponent },
  
  //  routes enterprises
  { path: 'all_enterprises', canActivate: [AuthGuard], component: EnterprisesComponent },
  { path: 'one_enterprise/:id', canActivate: [AuthGuard], component: OneEnterpriseComponent },
  { path: 'create_enterprise', canActivate: [AuthGuard], component: CreateEnterpriseComponent },
  { path: 'update_enterprise/:id', canActivate: [AuthGuard], component: UpdateEnterpriseComponent },
  
  //  contacts
  { path: 'send_mail', component: ContactComponent },
  
  //  routes static pages
  { path: 'mentions/:id', component: MentionsComponent },
  { path: 'about/:id', component: AboutComponent },
  
  //  routes user
  { path: 'all_users', canActivate: [AuthGuard], component: UsersComponent },
  { path: 'one_user/:id', canActivate: [AuthGuard], component: OneUserComponent },
  
  //  authentication
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'accueil', canActivate: [AuthGuard], component: HomeComponent },
  { path: '', redirectTo: 'accueil', pathMatch: 'full' }
  
  //  recherche pour tests
  // { path: 'search', canActivate: [AuthGuard], component: RechercheComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
