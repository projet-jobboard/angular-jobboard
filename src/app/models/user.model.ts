
//  déclaration de la classe User pour structurer les données récupérées via l'API

export class User {

  constructor(
    public email: string,
    public password: string,
    public firstname: string,
    public lastname: string,
    // public role_title: string 
    public fk_role_id
    
  ) {}
}


    
