
//  Déclaration de la classe Email pour structurer les données du formulaire

export class Email {

  constructor(
    public from: string,
    public subject: string,
    public message: string
    // public filename: string,
    // public path: string

  ) { }

}
