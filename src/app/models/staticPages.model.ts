
//  Déclaration de la classe StaticPages pour structurer les données de la bdd


export class StaticPages {

  constructor(
    public title: string,
    public content: Text,
    public image: string

  ) { }

}
