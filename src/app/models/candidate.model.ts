
//  Déclaration de la classe Candidate pour structurer les données de la bdd

export class Candidate {

  constructor(
    public fk_user_id: number,
    public cv: string

  ) { }

}
