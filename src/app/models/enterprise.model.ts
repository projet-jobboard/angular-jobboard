
//  déclaration de la classe Enterprise 
//  pour structurer les données récupérées via l'API
export class Enterprise {

  constructor(
    
    public name: string,
    public logo: string,
    public description: Text,
    public size: string,
    public website: string,
    public fk_user_id: number,
    public sector: {
      sector_name
    },
    public status: {
      status_type
    }

  ) {}
}

