
//  déclaration de la classe Offer 
//  comme format pour stocker les données

export class Offer {

  constructor(
    public title: string,
    public fk_enterprise_id: number,
    public detail: Text,
    public recruitment_url: string,
    public offer_location: string,
    public xp_required: string,
    public enterprise: {
      enterprise_id: number,
      name: string,
      logo: string,
      description: Text,
      size: string,
      website: string,
    },
    public contract_type: number,
    public sector_name: string,
    public status_type: string

  ) {}
}


    
