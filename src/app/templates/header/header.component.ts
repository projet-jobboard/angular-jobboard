
//  importer les ngModules
import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

//  importer les services
import {UserService} from '../../services/user.service';
import {CandidateService} from '../../services/candidate.service';
import {EnterpriseService} from '../../services/enterprise.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  siteTitle = 'Jobboard';
  siteLogo = "logo-j4t-1.png";

  user: any;
  candidate: any;
  enterprise: any;
  enterpriseObj: any;

  userValue: any;
  username: any;
  fname: string;
  lname: string;
  role: string;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private candidateService: CandidateService,
    private enterpriseService: EnterpriseService
    ) { }
    
    ngOnInit(): void {

      //  si un utilisateur est connecté on récupère ses infos du localStorage
      if(localStorage.length!=0) {

        this.userValue = JSON.parse(localStorage.getItem('current_user'));
        this.role = this.userValue.role;
        this.fname = this.userValue.firstname;
        this.lname = this.userValue.lastname;

        //  si le rôle est "candidat" on récupère ses infos de la bdd
        if(this.userValue.role === 'candidat') {
          this.getCandidate(this.route.snapshot.paramMap.get('id'));
          // console.log(`candidat : ${this.candidate.candidate_id}`);
        }

        //  si le rôle est "entreprise" on récupère ses infos de la bdd
        if(this.userValue.role==='entreprise') {
          console.log(this.userValue.role);
          this.get_user_by_username(
            this.route.snapshot.paramMap.get('this.fname'),
            this.route.snapshot.paramMap.get('this.lname')
          );
          // console.log(`fname : ${this.fname}`);
        }
      }
    }

  
  //  récupérer les infos du user à partir de son firstname et son lastname
  get_user_by_username(fname, lname) {
    this.userService.user_by_username(this.fname, this.lname).subscribe((result) => {
      this.username = result;
      
      this.get_ent_by_user(this.username.user_id);
      
    }, ((err) => {
      console.log(err);
    }))
  }
  
  //  récupérer le candidat à partir du username du user stocké dans le localStorage
  getCandidate(id) {
    //  user by username
    this.userService.user_by_username(this.userValue.firstname, this.userValue.lastname).subscribe((resUser) => {
      this.user = resUser;
      // console.log(resUser);
      // console.log(this.user.user_id);
      
      //  candidate by user_id
      this.candidateService.cand_by_user(this.user.user_id).subscribe((result) => {
        // console.log(result);
        this.candidate = result;
        // console.log(this.candidate.candidate_id);
      }, (err) => {
        console.log(err);
      })
    }, (err) => {
      console.log(err);
    })
  }

  //  récupérer l'entreprise à partir de l'id du user
  get_ent_by_user(user_id) {
    this.enterpriseService.enterprise_by_user(this.username.user_id).subscribe((result) => {
      this.enterpriseObj = result;
    }, ((err) => {
      console.log(err);
    }))
  }

  //  Déconnexion d'un user (candidat ou entreprise) : effacer tout le stockage local
  logout() {
    console.log('Tentative de déconnexion '+ Date());  // {{ ojd | shortDate}});
    
    localStorage.clear();
    console.log(localStorage.length);
    // localStorage.removeItem('user');  //  curUser
    this.router.navigate(['/register']);
    
    //  pipes : shortDate
  }

}
