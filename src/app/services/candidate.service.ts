
//  importer les NgModules dont on a besoin
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Candidate} from '../models/candidate.model';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {

  uri = 'http://localhost:3000/candidate';

  constructor(
    private http: HttpClient
  ) { }

  all_candidates() {
    return this.http.get<Candidate[]>(`${this.uri}/all`);
  }

  one_candidate(id: number) {
    return this.http.get<Candidate>(`${this.uri}/one/${id}`);
  }

  create_candidate(data: Candidate) {
    return this.http.post<Candidate>(`${this.uri}/create`, data);
  }

  update_candidate(id: number, data: Candidate) {
    return this.http.put<Candidate>(`${this.uri}/update/${id}`, data);
  }

  deactivate_candidate(id: number, data: Candidate) {
    return this.http.put<Candidate>(`${this.uri}/deactivate/${id}`, data);
  }

  //  candidat par user
  cand_by_user(user: number) {
    return this.http.get<Candidate>(`${this.uri}/cand/${user}`);
  }

}
