
//  importer les modèles dont on aura besoin
import {Offer} from '../models/offer.model';

//  importer le module "injectable" pour utiliser les "dependancy injections"
import { Injectable } from '@angular/core';
//  importer HttpClient pour utiliser les adresses url
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class OfferService {

  uri = 'http://localhost:3000/offer';

  constructor(
    private http: HttpClient
  ) { }

  all_offers() {
    return this.http.get<Offer[]>(`${this.uri}/all`);
  }

  one_offer(id: number) {
    return this.http.get<Offer>(`${this.uri}/one/${id}`);
  }

  create_offer(data: Offer) {
    return this.http.post<Offer>(`${this.uri}/create`, data);
  }

  update_offer(id: number, data: Offer) {
    return this.http.put<Offer>(`${this.uri}/update/${id}`, data);
  }

  deactivate_offer(id: number, data: Offer) {
    return this.http.put<Offer>(`${this.uri}/deactivate/${id}`, data);
  }

  //  offres par entreprise
  offers_by_ent(ent: number) {
    return this.http.get<Offer[]>(`${this.uri}/by_ent/${ent}`);
  }

  /*******************************************/
  /*************** FORM RECHERCHE ************/
  /*******************************************/

  offers_by_contract(contract: number) {
    return this.http.get<Offer[]>(`${this.uri}/by_contract/${contract}`);
  }

  offers_by_sector(sector: number) {
    return this.http.get<Offer[]>(`${this.uri}/by_sector/${sector}`);
  } 

  offers_by_status(status: number) {
    return this.http.get<Offer[]>(`${this.uri}/by_status/${status}`);
  }

  offers_by_keyword(kw: string) {
    return this.http.get<Offer[]>(`${this.uri}/by_keyword/${kw}`);
  }

  offers_by_many_choices(kw: string, contract: number, sector: number, status: number) {
    return this.http.get<Offer[]>(`${this.uri}/many_choices/${kw}/${contract}/${sector}/${status}`);
  }


}
