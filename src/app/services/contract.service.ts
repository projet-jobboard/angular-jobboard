
import { Injectable } from '@angular/core';
//  import du modèle dont on aura besoin
import {Contract} from '../models/contract.model';
//  import du NgModule HttpClient pour utiliser les url
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  uri = 'http://localhost:3000/contract';

  constructor(
    private http: HttpClient
  ) { }

  all_contracts() {
    return this.http.get<Contract[]>(`${this.uri}/all`);
  }

  one_contract(id: number) {
    return this.http.get<Contract>(`${this.uri}/one/${id}`);
  }

  create_contract(data: Contract) {
    return this.http.post<Contract>(`${this.uri}/create`, data);
  }

  update_contract(id: number, data: Contract) {
    return this.http.put<Contract>(`${this.uri}/update/${id}`, data);
  }

}





