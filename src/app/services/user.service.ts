
//  importer les NgModules dont on a besoin
import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';

//  importer le modèle dont on a besoin
import {User} from '../models/user.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  uri = 'http://localhost:3000/user';

  constructor(
    private http: HttpClient
  ) { }

  all_users() {
    return this.http.get<User[]>(`${this.uri}/all`);
  }

  one_user(id: number) {
    return this.http.get<User>(`${this.uri}/one/${id}`);
  }

  user_by_username(fname: string, lname: string) {
    return this.http.get<User>(`${this.uri}/user_by_username/${fname}/${lname}`);
  }

  create_user(data: User) {
    return this.http.post<User>(`${this.uri}/create`, data);
  }

  update_user(id: number, data: User) {
    return this.http.put<User>(`${this.uri}/update/${id}`, data);
  }

  deactivate_user(id: number, data: User) {
    return this.http.put<User>(`${this.uri}/deactivate/${id}`, data);
  }

}
