
//  import des NgModules dont on a besoin
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

//  import du model
import {StaticPages} from '../models/staticPages.model';


@Injectable({
  providedIn: 'root'
})
export class StaticPagesService {

  uri = 'http://localhost:3000/pages';

  constructor(
    private http: HttpClient
  ) { }

  all_pages() {
    return this.http.get<StaticPages[]>(`${this.uri}/all`);
  }

  one_page(id: number) {
    return this.http.get<StaticPages>(`${this.uri}/one/${id}`);
  }

  create_page(data: StaticPages) {
    return this.http.post<StaticPages>(`${this.uri}/create`, data);
  }

  update_page(id: number, data: StaticPages) {
    return this.http.put<StaticPages>(`${this.uri}/update/${id}`, data);
  }

  delete_page(id: number) {
    return this.http.delete<StaticPages>(`${this.uri}/delete/${id}`);
  }

}
