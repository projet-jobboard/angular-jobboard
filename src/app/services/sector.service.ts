
import { Injectable } from '@angular/core';
//  import du modèle dont on aura besoin
import {Sector} from '../models/sector.model';
//  import du NgModule HttpClient pour utiliser les url
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SectorService {

  uri = 'http://localhost:3000/sector';

  constructor(
    private http: HttpClient
  ) { }
  

  all_sectors() {
    return this.http.get<Sector[]>(`${this.uri}/all`);
  }

  one_sector(id: number) {
    return this.http.get<Sector>(`${this.uri}/one/${id}`);
  }

  create_sector(data: Sector) {
    return this.http.post<Sector>(`${this.uri}/create`, data);
  }

  update_sector(id: number, data: Sector) {
    return this.http.put<Sector>(`${this.uri}/update/${id}`, data);
  }


}

