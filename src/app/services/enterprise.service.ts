
//  importer les modèles dont on aura besoin
import {Enterprise} from '../models/enterprise.model';

//  importer le module "injectable" pour utiliser les "dependancy injections"
import { Injectable } from '@angular/core';
//  importer HttpClient pour utiliser les adresses url
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class EnterpriseService {

  uri = 'http://localhost:3000/enterprise';

  constructor(
    private http: HttpClient
  ) { }

  all_enterprises() {
    return this.http.get<Enterprise[]>(`${this.uri}/all`);
  }

  one_enterprise(id: number) {
    return this.http.get<Enterprise>(`${this.uri}/one/${id}`);
  }

  enterprise_by_user(user_id: number) {
    return this.http.get<Enterprise>(`${this.uri}/user/${user_id}`);
  }

  create_enterprise(data: Enterprise) {
    return this.http.post<Enterprise>(`${this.uri}/create`, data);
  }

  update_enterprise(id: number, data: Enterprise) {
    return this.http.put<Enterprise>(`${this.uri}/update/${id}`, data);
  }

  deactivate_enterprise(id: number, data: Enterprise) {
    return this.http.put<Enterprise>(`${this.uri}/deactivate/${id}`, data);
  }

}
