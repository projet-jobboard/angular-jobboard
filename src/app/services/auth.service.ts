
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {User} from '../models/user.model';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  uri = 'http://localhost:3000/user';
  
  constructor(
    private http: HttpClient
  ) { }

  //  toutes les méthodes disponibles dans ce service sont définies ici :

  register(data: User) {
    return this.http.post<User>(`${this.uri}/create`, data);
  }

  login(email: string, pwhash: string) {
    console.log(email);
    console.log(pwhash);
    return this.http.post<User>(`${this.uri}/login`, {email, pwhash});
  }

}

