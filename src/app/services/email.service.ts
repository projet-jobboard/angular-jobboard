
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  uri = 'http://localhost:3000/email';

  constructor(
    private http: HttpClient
  ) { }

  send_mail(data) {
    return this.http.post(`${this.uri}/contact`, data);
  }

  //   create_candidate(data: Candidate) {
  //   return this.http.post<Candidate>(`${this.uri}/create`, data);
  // }

}
