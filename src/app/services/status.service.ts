
import { Injectable } from '@angular/core';
//  import du modèle dont on aura besoin
import {Status} from '../models/status.model';
//  import du NgModule HttpClient pour utiliser les url
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  uri = 'http://localhost:3000/status';

  constructor(
    private http: HttpClient
  ) { }

  all_status() {
    return this.http.get<Status[]>(`${this.uri}/all`);
  }

  one_status(id: number) {
    return this.http.get<Status>(`${this.uri}/one/${id}`);
  }

  create_status(data: Status) {
    return this.http.post<Status>(`${this.uri}/create`, data);
  }

  update_status(id: number, data: Status) {
    return this.http.put<Status>(`${this.uri}/update/${id}`, data);
  }

}




