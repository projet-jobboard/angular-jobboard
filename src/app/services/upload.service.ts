
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UploadService {

  uri = 'http://localhost:3000/upload';

  constructor(
    private http: HttpClient
  ) { }

  // upload_file() {
  //   return this.http.post(`${this.uri}/`, upload.single('upfile'));
  // }

  public upload_file(file) {
    this.http.post(`${this.uri}/`, file);
  }


}

