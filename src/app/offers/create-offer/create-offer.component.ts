
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

//  importer les modèles dont on aura besoin
import {Contract} from '../../models/contract.model';
import {Enterprise} from '../../models/enterprise.model';
import {Offer} from '../../models/offer.model';
//  importer les services dont on aura besoin
import {ContractService} from '../../services/contract.service';
import {EnterpriseService} from '../../services/enterprise.service';
import {OfferService} from 'src/app/services/offer.service';
import {UserService} from '../../services/user.service';


@Component({
  selector: 'app-create-offer',
  templateUrl: './create-offer.component.html',
  styleUrls: ['./create-offer.component.css']
})
export class CreateOfferComponent implements OnInit {

  pageTitle = 'Créer une offre';

  //  données utiles pour compléter le formulaire (selects)
  contracts: Contract[];

  //  données attendues, d'après l'objet offerModel
  offer: Offer = {
    title: '',
    fk_enterprise_id: null,
    detail: null,
    recruitment_url: '',
    offer_location: '',
    xp_required: '',
    fk_contract_id: '',
    fk_sector_id: '',
    fk_status_id: '',
    enterprise: {
      enterprise_id: null,
      name: '',
      logo: '',
      description: null,
      size: '',
      website: '',
    },
    contract_type: null
  };

  isSubmitted = false;

  user = null;

  userValue: any;
  username: any;
  fname: string;
  lname: string;
  role: string;

  enterprise: any;

  constructor(
    private contractService: ContractService,
    private enterpriseService: EnterpriseService,
    private offerService: OfferService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    //  données pour remplir le formulaire (selects)
    this.contractService.all_contracts().subscribe((contract) => {
      this.contracts = contract;
      console.log(this.contracts[0]);
    });

    //  récupérer l'utilisateur connecté
    this.userValue = JSON.parse(localStorage.getItem('current_user'));
    console.log(this.userValue);

    this.role = this.userValue.role;
    this.fname = this.userValue.firstname;
    this.lname = this.userValue.lastname;

    //  récupérer les infos du user par son prénom et nom
    this.get_user_by_username(
      this.route.snapshot.paramMap.get('this.fname'),
      this.route.snapshot.paramMap.get('this.lname')
    );

  }


  //  récupérer les infos du user à partir de son firstname et son lastname
  get_user_by_username(fname, lname) {
    this.userService.user_by_username(this.fname, this.lname).subscribe((result) => {
      this.username = result;
      console.log(`username.user_id : ${this.username.user_id}`);
      this.get_ent_by_user(this.username.user_id);
      console.log(`ent_by_user_id : ${this.enterprise.enterprise_id}`);
    }, ((err) => {
      console.log(err);
    }))
  }

  //  récupérer l'entreprise à partir de l'id du user
  get_ent_by_user(user_id) {
    this.enterpriseService.enterprise_by_user(this.username.user_id).subscribe((result) => {
      this.enterprise = result;
      console.log(`enterprise.enterprise_id : ${this.enterprise.enterprise_id}`);
    }, ((err) => {
      console.log(err);
    }))
  }

  createOffer() {
    const data = {
      title: this.offer.title,
      fk_enterprise_id: this.offer.fk_enterprise_id,
      detail: this.offer.detail,
      recruitment_url: this.offer.recruitment_url,
      offer_location: this.offer.offer_location,
      xp_required: this.offer.xp_required,
      enterprise: {
        enterprise_id: this.offer.enterprise.enterprise_id,
        name: this.offer.enterprise.name,
        logo: this.offer.enterprise.logo,
        description: this.offer.enterprise.description,
        size: this.offer.enterprise.size,
        website: this.offer.enterprise.website,
      },
      contract_type: this.offer.contract_type,
      sector_name: this.offer.sector_name,
      status_type: this.offer.status_type
    }
    console.log(data);

    //  enregistrer le contenu de data dans la bdd via le service et l'API
    this.offerService.create_offer(data).subscribe(() => {
      this.isSubmitted = true;
      console.log(`data envoyé : ${data}`);
    }, (err) => {
      console.log(err);
    });

  }

}
