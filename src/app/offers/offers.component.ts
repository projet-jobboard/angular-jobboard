
//  importer les modules pour gérer l'utilisation des components 
//  et le lancement de l'application
import { Component, OnInit } from '@angular/core';
//  importer le service dont on aura besoin
import {OfferService} from '../services/offer.service';
//  importer le modèle dont on aura besoin
import {Offer} from '../models/offer.model';


@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['../app.component.css']
})
export class OffersComponent implements OnInit {

  //  variable :
  pageTitle = 'Toutes nos offres';

  //  typage :
  count: number;
  offers: Offer[];

  constructor(
    private offerService: OfferService
  ) { }

  ngOnInit(): void {
    this.offerService.all_offers().subscribe((result) => {
      this.offers = result;
      this.count = result.length;
      console.log(result);
    });

  }

}
