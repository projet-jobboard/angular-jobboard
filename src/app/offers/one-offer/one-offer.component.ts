
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

//  import du service dont on aura besoin
import {OfferService} from '../../services/offer.service';


@Component({
  selector: 'app-one-offer',
  templateUrl: './one-offer.component.html'
})
export class OneOfferComponent implements OnInit {

  pageTitle = 'Détail';

  userValue: any;
  offer: any;

  constructor(
    private offerService: OfferService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    
    this.userValue = JSON.parse(localStorage.getItem('current_user'));
    this.get_offer(this.route.snapshot.paramMap.get('id'));
  }

  //  récupérer les données de l\'offre à partir de son id
  get_offer(id) {
    this.offerService.one_offer(id).subscribe((result) => {
      this.offer = result;
      console.log(result);
      console.log(this.offer.enterprise.size);
    }, (err) => {
      console.log(err);
    })
  }

}
