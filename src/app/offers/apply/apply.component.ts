
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
//  importer les services
import {EmailService} from '../../services/email.service';
import {OfferService} from '../../services/offer.service';
import {UserService} from '../../services/user.service';
//  importer les modèles
import {Email} from '../../models/email.model';


@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.css']
})
export class ApplyComponent implements OnInit {

  pageTitle = 'Postuler';

  email: Email = {
    from: '',
    subject: '',
    message: ''
    // filename: '',
    // path: ''
  };

  offer: any;
  user: any;

  userValue: any;
  fname: string;
  lname: string;
  username: any;

  userEmail: string;

  isSubmit = false;

  constructor(
    private emailService: EmailService,
    private route: ActivatedRoute,
    private router: Router,
    private offerService: OfferService,
    private userService: UserService
  ) { }

  ngOnInit(): void {

    //  récupérer les infos du user connecté stockées dans le localStorage
    this.userValue = JSON.parse(localStorage.getItem('current_user'));
    console.log(`current_user : ${this.userValue.firstname}`);  //  pour vérif

    //  stocker le firstname et le lastname pour les utiliser dans get_user_by_username
    this.fname = this.userValue.firstname;
    this.lname = this.userValue.lastname;

    //  récupérer les infos du user à partir de son firstname et lastname
    this.get_user_by_username(
      this.route.snapshot.paramMap.get('this.fname'),
      this.route.snapshot.paramMap.get('this.lname')
    );
    // console.log(this.username.email);  //  vérif qu'on a bien le mail du user pour le mail

    //  récupérer l'id de l'offre pour le mail
    this.get_offer(this.route.snapshot.paramMap.get('id'));

  }

  //  récupérer les données de l'offre à partir de l'url
  get_offer(id) {
    this.offerService.one_offer(id).subscribe((result) => {
      this.offer = result;
      console.log(result);
    }, (err) => {
      console.log(err);
    })
  }

  //  récupérer les infos du user à partir de son firstname et son lastname
  get_user_by_username(fname, lname) {
    this.userService.user_by_username(this.fname, this.lname).subscribe((result) => {
      this.username = result;
    
    }, ((err) => {
      console.log(err);
    }))
  }

  sendMail() {
    this.get_offer(this.route.snapshot.paramMap.get('id'))
    const data = {
      from: this.username.email,
      subject: this.offer.title+' '+this.offer.offer_id,
      message: this.email.message
      // filename: this.email.filename,
      // path: this.email.path
    };
    console.log(data.subject);

    //  pour vérif qu'on récupère les données du formulaire
    console.log(data);

    this.emailService.send_mail(data).subscribe(() => {
      this.isSubmit = true;
      //  vérifie que les données dans data sont toujours là, en cas d'échec
      console.log(data);
    }, (err) => {
      console.log(err);
    })
  }

}
