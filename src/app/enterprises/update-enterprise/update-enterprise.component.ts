
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

//  import du service dont on aura besoin
import {OfferService} from '../../services/offer.service';
import {EnterpriseService} from '../../services/enterprise.service';
import { SectorService } from 'src/app/services/sector.service';
import { StatusService } from 'src/app/services/status.service';

//  import models
import {Sector} from '../../models/sector.model';
import {Status} from '../../models/status.model';
import {Enterprise} from '../../models/enterprise.model';

@Component({
  selector: 'app-update-enterprise',
  templateUrl: './update-enterprise.component.html',
  styleUrls: ['../../app.component.css']
})
export class UpdateEnterpriseComponent implements OnInit {

  pageTitle : 'Modifier une entreprise';

  isSubmit = false;

  userValue: any;
  offer: any;
  ent: any;

  count_sectors: number;
  count_status: number;

  //  variables pour afficher formulaire
  sectors: Sector[];
  statuss: Status[];

  sector: Sector = {
    sector_name: ''
  }
  status: Status = {
    status_type: ''
  }

  //  données attendues, d'après l'objet enterpriseModel
  enterprise: Enterprise = {
    name: '',
    logo: '',
    description: null,
    size: '',
    website: '',
    sector: null,
    status: null,
    fk_user_id: null
  };


  constructor(
    private enterpriseService: EnterpriseService,
    private sectorService: SectorService,
    private statusService: StatusService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.userValue = JSON.parse(localStorage.getItem('current_user'));

    //  récupérer l'entreprise par son id dans l'url
    this.get_enterprise(this.route.snapshot.paramMap.get('id'));

    this.sectorService.all_sectors().subscribe((sector) => {
      this.sectors = sector;
      this.count_sectors = this.sectors.length;
    });

    this.statusService.all_status().subscribe((status) => {
      this.statuss = status;
      this.count_status = this.statuss.length;
    })

  }

      /*  cinegular

    //  get the movie by its id
    one_movie(id) {
      this.movieService.get_one_movie(id).subscribe((movie) => {
        this.movie = movie;
      }, (err) => {
        console.log(err);
      })
    };
    */

  //  récupérer l'enterprise à partir de son id
  get_enterprise(ent_id) {
    this.enterpriseService.one_enterprise(ent_id).subscribe((result) => {
      this.ent = result;

    }, ((err) => {
      console.log(err);
    }))
  };


  //  modifier l'entreprise à partir de son id
  saveEnterprise() {
    //  stocker les données de l'entreprise dans 'data' suivant la structure de 'enterprise'
    const dataEnterprise = {
      name: this.enterprise.name,
      logo: this.enterprise.logo,
      description: this.enterprise.description,
      size: this.enterprise.size,
      website: this.enterprise.website,
      sector: this.enterprise.sector.sector_name,
      status: this.enterprise.status.status_type,
      fk_user_id: this.enterprise.fk_user_id
    }

    const id = this.ent.id;

    this.enterpriseService.update_enterprise(id, dataEnterprise).subscribe(() => {
      this.isSubmit = true;
      console.log(dataEnterprise);
    }, (err) => {
      console.log(err);
    });
  }


}
