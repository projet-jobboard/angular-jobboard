import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneEnterpriseComponent } from './one-enterprise.component';

describe('OneEnterpriseComponent', () => {
  let component: OneEnterpriseComponent;
  let fixture: ComponentFixture<OneEnterpriseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneEnterpriseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneEnterpriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
