
//  importer les modules pour gérer l'utilisation des components 
//  et le lancement de l'application
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

//  importer les services dont on aura besoin
import {EnterpriseService} from '../../services/enterprise.service';
import { UserService } from '../../services/user.service';
import {OfferService} from '../../services/offer.service';
import {CandidateService} from '../../services/candidate.service';
//  importer les modèles dont on aura besoin
import {Offer} from '../../models/offer.model';
import {Candidate} from '../../models/candidate.model';


@Component({
  selector: 'app-one-enterprise',
  templateUrl: './one-enterprise.component.html',
  styleUrls: ['../../app.component.css']
})
export class OneEnterpriseComponent implements OnInit {

  pageTitle = 'Fiche entreprise';

  role = 'enterprise';

  userValue: any;
  fname: string;
  lname: string;
  enterpriseObj: any;
  ent_id: any;
  enterprise: any;
  username: any;
  userId: number;

  offers: Offer[];

  candidates: Candidate[];

  constructor(
    private enterpriseService: EnterpriseService,
    private userService: UserService,
    private route: ActivatedRoute,
    private OfferService: OfferService,
    private candidateService: CandidateService
  ) { }

  ngOnInit(): void {

    if(localStorage.length!=0) {
      this.userValue = JSON.parse(localStorage.getItem('current_user'));
      console.log(`current_user : ${this.userValue.firstname}`);  //  pour vérif
    }

    //  récupérer l'entreprise par son id dans l'url
    this.get_enterprise(this.route.snapshot.paramMap.get('id'));

    //  récupérer fname et lname depuis localStorage
    this.userValue = JSON.parse(localStorage.getItem('current_user'));
    this.fname = this.userValue.firstname;
    this.lname = this.userValue.lastname;
    
    //  récupérer user by username
    //  puis entreprise par l'id du user
    //  Pour vérifier si le user connecté représente l'entreprise ou pas
    //  pour afficher les boutons modifier et supprimer, par exemple
    this.get_user_by_username(
      this.route.snapshot.paramMap.get('this.fname'),
      this.route.snapshot.paramMap.get('this.lname')
    );
    
    //  récupérer les offres à partir de l'id de l'entreprise
    this.get_offers_by_ent(this.route.snapshot.paramMap.get('id'));

    this.candidateService.all_candidates().subscribe((result) => {
      this.candidates = result;
      console.log(result);
    }, (err) => {
      console.log(err);
    })

  }

  //  récupérer les infos du user à partir de son firstname et son lastname
  get_user_by_username(fname, lname) {
    this.userService.user_by_username(this.fname, this.lname).subscribe((result) => {
      this.username = result;

      this.get_ent_by_user(this.username.user_id);
      
    }, ((err) => {
      console.log(err);
    }))
  }
  
  //  récupérer l'entreprise à partir de l'id du user
  get_ent_by_user(user_id) {
    this.enterpriseService.enterprise_by_user(this.username.user_id).subscribe((result) => {
      this.enterpriseObj = result;

    }, ((err) => {
      console.log(err);
    }))
  }
  
  //  récupérer l'enterprise à partir de son id
  get_enterprise(ent_id) {
    this.enterpriseService.one_enterprise(ent_id).subscribe((result) => {
      this.enterprise = result;

    }, ((err) => {
      console.log(err);
    }))
  };

  //  récupérer toutes les offres de l'entreprise
  get_offers_by_ent(ent_id) {
    this.OfferService.offers_by_ent(ent_id).subscribe((resOffs) => {
      this.offers = resOffs;
      console.log(resOffs);
      console.log(`offers length : ${this.offers.length}`);
    })
  }



}


