
//  importer les modules pour gérer l'utilisation des components 
//  et le lancement de l'application
import { Component, OnInit } from '@angular/core';
//  importer le service dont on aura besoin
import {EnterpriseService} from '../services/enterprise.service';
//  importer les modèles dont on aura besoin
import {Enterprise} from '../models/enterprise.model';


@Component({
  selector: 'app-enterprises',
  templateUrl: './enterprises.component.html'/*,
  styleUrls: ['./enterprises.component.css']*/
})
export class EnterprisesComponent implements OnInit {
  
  //  variable :
  pageTitle = 'Toutes nos entreprises partenaires';

  //  typage :
  enterprises: Enterprise[];

  
  constructor(
    private enterpriseService: EnterpriseService
  ) { }

  ngOnInit(): void {
    this.enterpriseService.all_enterprises().subscribe((result) => {
      this.enterprises = result;
      console.log(result);
      // console.log(this.enterprises.sector_name);
    })
  }

}
