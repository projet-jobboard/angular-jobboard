
import { Component, OnInit } from '@angular/core';
//  importer les mNgModules d'utilisation des routes
import {Router} from '@angular/router';

//  importer les modèles dont on aura besoin
import {Enterprise} from '../../models/enterprise.model';
import {Contract} from '../../models/contract.model';
import {Sector} from '../../models/sector.model';
import {Status} from '../../models/status.model';
//  importer les services dont on aura besoin
import {EnterpriseService} from '../../services/enterprise.service';
import {ContractService} from '../../services/contract.service';
import {SectorService} from '../../services/sector.service';
import {StatusService} from '../../services/status.service';
import {UserService} from '../../services/user.service';


@Component({
  selector: 'app-create-enterprise',
  templateUrl: './create-enterprise.component.html',
  styleUrls: ['./create-enterprise.component.css']
})
export class CreateEnterpriseComponent implements OnInit {

  //  données utiles pour compléter le formulaire (selects)
  contracts: Contract[];
  sectors: Sector[];
  statuss: Status[];

  contract: Contract = {
    contract_type: ''
  }
  sector: Sector = {
    sector_name: ''
  }
  status: Status = {
    status_type: ''
  }

  //  données attendues, d'après l'objet enterpriseModel
  enterprise: Enterprise = {
    name: '',
    logo: '',
    description: null,
    size: '',
    website: '',
    sector: null,
    status: null,
    fk_user_id: null
  };


  isSubmitted = false;

  user = null;

  userValue: any;

  pageTitle = 'Créer une fiche entreprise';

  constructor(
    private enterpriseService: EnterpriseService,
    private contractService: ContractService,
    private sectorService: SectorService,
    private statusService: StatusService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.sectorService.all_sectors().subscribe((sector) => {
      this.sectors = sector;
      console.log(this.sectors[0]);
    });

    this.statusService.all_status().subscribe((status) => {
      this.statuss = status;
    })

    this.userValue = JSON.parse(localStorage.getItem('current_user'));
    console.log(this.userValue);
    
  }

  getUser(user_id) {
    this.userService.one_user(user_id).subscribe((result) => {
      this.user = result;
    }, (err) => {
      console.log(err);
    });
  }

  createEnterprise() {
    const data = {
      name: this.enterprise.name,
      logo: this.enterprise.logo,
      description: this.enterprise.description,
      size: this.enterprise.size,
      website: this.enterprise.website,
      sector: this.enterprise.sector.sector_name,
      status: this.enterprise.status.status_type,
      fk_user_id: this.enterprise.fk_user_id
    }
    console.log(data);  //  pour vérifier le contenu de data

    //  enregistrer le contenu de data dans la bdd
    //  via le service et l'API
    this.enterpriseService.create_enterprise(data).subscribe(() => {
      this.isSubmitted = true;
      console.log(data);
    }, (err) => {
      console.log(err);
    });
  }


}
