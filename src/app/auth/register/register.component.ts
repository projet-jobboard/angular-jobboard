
//  importer les modules pour gérer l'utilisation des components 
//  et le lancement de l'application
import { Component, OnInit } from '@angular/core';
//  importer les mNgModules d'utilisation des routes
import {ActivatedRoute, Router} from '@angular/router';

//  importer les services dont on aura besoin
import {AuthService} from '../../services/auth.service';
//  importer les modèles dont on aura besoin
import {User} from '../../models/user.model';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  //  prorpiétés attendues pour l'objet user, d'après le modèle User
  user: User = {
    email: '',
    password: '',
    firstname: '',
    lastname: '' ,
    fk_role_id: ''
  };
  isSubmit = false;

  pageTitle = 'Inscription';

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute, 
    private router : Router
  ) { }

  ngOnInit(): void {
  }


  //  méthode appelée par le clic sur submit
  saveUser() {
  //  stocker les infos du formulaire dans la variable 'data',
  //  depuis la variable 'user'
    const data = {
      email: this.user.email,
      password: this.user.password,
      firstname: this.user.firstname ,
      lastname: this.user.lastname,
      fk_role_id: this.user.fk_role_id
    };
    
    console.log(data);  //  pour vérifier le contenu de data
    
    //  enregistrer les données stockées dans 'data' dans la bdd 
    //  via le service et l'API
    this.authService.register(data).subscribe(() => {
      this.isSubmit = true;
      console.log(data);
      this.router.navigate(['/accueil']);
    }, (err) => {
      console.log(err);
    });  
  }

}



