
import { Component, OnInit } from '@angular/core';

//  importer les mNgModules d'utilisation des routes
import {ActivatedRoute, Router} from '@angular/router';

//  importer le service dont on aura besoin
import {AuthService} from '../../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  email: string;
  pwhash: string;

  firstname: string;
  lastname: string;

  current_user: any = {};
  
  pageTitle = 'Connexion';
 
  constructor(
    private authService: AuthService,
    private router : Router
  ) { }

  ngOnInit(): void {
  }

  //  envoi du form appelle la méthode login_user()
  login_user() {
    //  récupérer la réponse de l'API via le service authService
    this.authService.login(this.email, this.pwhash).subscribe((result) => {
      this.current_user = result;
      // stocker la réponse dans localStorage
      const name = this.current_user.firstname+' '+this.current_user.lastname;
      localStorage.setItem('current_user', JSON.stringify(this.current_user));
      
      if(localStorage) {
        this.router.navigate(['/accueil']);
        return this.current_user;
      } else {
        console.log("erreur localstorage");
      }
    }, (err) => {
      console.log(err);
    })
  };
  
}
