
//  importer les modules pour gérer l'utilisation des components 
//  et le lancement de l'application
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

//  importer le service dont on aura besoin
import { UserService } from '../../services/user.service';
// import {EnterpriseService} from '../../services/enterprise.service';


@Component({
  selector: 'app-one-user',
  templateUrl: './one-user.component.html',
  styleUrls: ['./one-user.component.css']
})
export class OneUserComponent implements OnInit {

  pageTitle = 'Vue de l\'utilisateur';

  user: any;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.get_user(this.route.snapshot.paramMap.get('id'));
  }

  //  récupérer les données du user à partir de son id
  get_user(id) {
    this.userService.one_user(id).subscribe((result) => {
      this.user = result;
      console.log(`user : ${this.user}`);
    }, (err) => {
      console.log(err);
    })
  }

}




