
import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';

import { User } from '../models/user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  pageTitle = "Tous les utilisateurs";

  users: User[];
  count: number;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.all_users().subscribe((result) => {
      this.users = result;
      this.count = result.length;
      console.log(result);
    }, (err) => {
      console.log(err);
    });

  }

}
